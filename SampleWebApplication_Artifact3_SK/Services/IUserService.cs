﻿using SampleWebApplication_Artifact3_SK.Models;

namespace SampleWebApplication_Artifact3_SK.Services
{
    public interface IUserService
    {
        Task<UserDetails> GetUserDetails(int id);
        Task<CreateUserResponse> CreateUser(CreateUserRequest createUserRequest);

        Task<LoginUserResponse> LoginUser(LoginUserRequest loginUserRequest);
    }
}
